package by.es.calculator;
/*
 * Copyright (c) 2017, Expert Soft Inc. <expert-soft.by/>
 *
 * All portions of the code written by Expert Soft Inc. are property of Expert Soft Inc. 
 * All rights reserved.
 * 
 * Expert Soft Inc.
 * Minsk, Belarus
 * Web: expert-soft.by
 */

import junit.framework.TestCase;

/**
 * by.es.calculator.CalculatorTest.
 *
 * @author Maksim Piatrou <maksim.piatrou@expert-soft.by>
 * @package by.es.calculator
 * @link http://expert-soft.by/
 * @copyright 2017 Expert Soft Inc.
 */
public class CalculatorTest extends TestCase
{
	public void testExpression() {
		Calculator calculator = new Calculator();

		String expression1 = "1+1+3*4*2-2*2+6/3";
		Node node = calculator.evaluate(expression1);

		assertEquals(24.0, node.evaluate());
		assertEquals("1.0 + 1.0 + 3.0 * 4.0 * 2.0 - 2.0 * 2.0 + 6.0 / 3.0" , node.toString());

		String expression2 = "1+2-3-4";
		node = calculator.evaluate(expression2);

		assertEquals(-4.0, node.evaluate());
		assertEquals("1.0 + 2.0 - 3.0 - 4.0" , node.toString());
	}
}
