/*
 * Copyright (c) 2017, Expert Soft Inc. <expert-soft.by/>
 *
 * All portions of the code written by Expert Soft Inc. are property of Expert Soft Inc. 
 * All rights reserved.
 * 
 * Expert Soft Inc.
 * Minsk, Belarus
 * Web: expert-soft.by
 */
package by.es.calculator;

/**
 * Node.
 *
 * @author Maksim Piatrou <maksim.piatrou@expert-soft.by>
 * @package by.es.calculator
 * @link http://expert-soft.by/
 * @copyright 2017 Expert Soft Inc.
 */
public class Node
{
	private char operator;
	private double value;
	Node left;
	Node right;

	public Node(Node left, Node right, char operator, double value) {
		this.left = left;
		this.right = right;
		this.operator = operator;
		this.value = value;
	}

	public double evaluate() {
		switch (operator) {
			case '+':
				return left.evaluate() + right.evaluate();
			case '-':
				return left.evaluate() - right.evaluate();
			case '*':
				return left.evaluate() * right.evaluate();
			case '/':
				return left.evaluate() / right.evaluate();
			default:
				return value;
		}
	}

	public String toString() {
		switch (operator) {
			case '+':
				return left.toString() + " + " + right.toString();
			case '-':
				return left.toString() + " - " + right.toString();
			case '*':
				return left.toString() + " * " + right.toString();
			case '/':
				return left.toString() + " / " + right.toString();
			default:
				return String.valueOf(value);
		}
	}
}
